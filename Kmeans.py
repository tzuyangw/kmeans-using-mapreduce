from pyspark import SparkContext, SparkConf 
import math
import numpy as np
import matplotlib.pyplot as plt
import csv

MODE = 'e' # 'e' for euclidean and 'm' for manhattan

k = 10
col = 58
iteration = 20

conf = SparkConf().setMaster("local").setAppName("MDA_hw3")
sc = SparkContext(conf = conf)

def Euclidean(point1, point2):
    dist = 0.0
    for i in range(col):
        dist += (point1[i] - point2[i]) ** 2

    # Return both cost and distance value
    return dist, math.sqrt(dist)

def Manhattan(point1, point2):
    dist = 0.0
    for i in range(col):
        dist += abs(point1[i] - point2[i])
    
    # Return both cost and distance value
    return dist, dist

def clusterer(node):
    global centroids
    dist_array = np.zeros(k)
    cost_array = np.zeros(k)
    for i, centroid in enumerate(centroids):
        if (MODE == "e"):
            cost_array[i], dist_array[i] = Euclidean(node, centroid)
        elif (MODE == "m"):
            cost_array[i], dist_array[i] = Manhattan(node, centroid)

    # Find the closest centroid and allocate node to it
    clusterIndex = dist_array.argmin()
    cost = cost_array.min()

    # Return the cluster id and the cost of the node
    return (clusterIndex, (node, cost))

def init_centroid(data):
    print("Initialize the centroids")

    global centroids
    centroids = []
    for centroid in data:
        centroids.append(centroid)

def loss_before_kmeans(nodes):
    print("Calculate the first loss before k-means algorithm")

    global centroids
    cost = 0.0
    cost_array = np.zeros(k)

    # Calculate the cost value before k_means algorithm
    for node in nodes:
        for i, centroid in enumerate(centroids):
            if(MODE == 'e'):
                cost_array[i], _ = Euclidean(node, centroid)
            elif(MODE == 'm'):
                cost_array[i], _ = Manhattan(node, centroid)
        cost += cost_array.min()
    return cost

def k_means(nodes, k, iteration, file_name):
    global centroids
    costs = np.zeros(iteration + 1) # Need to calculate iteration + 1 times losses

    centroid_info = np.loadtxt(file_name + ".txt", usecols=range(0, col), delimiter=' ', dtype=np.float32)
    init_centroid(centroid_info)

    # The first loss calcilation before K-means algorithm
    costs[0] = loss_before_kmeans(nodes)

    data = sc.parallelize([node for node in nodes])

    for i in range(1, iteration + 1):
        print("Round " + str(i))
        
        # Use clusterer mapper to cluster the nodes and return the data format (clutser_id, (node, cost))
        # and split the node and cost information into clustered_node and cnetroids_cost separately
        cluster_data = data.map(clusterer)
        clustered_node = cluster_data.map(lambda x: (x[0], x[1][0]))
        centroids_cost = cluster_data.map(lambda x: (x[0], x[1][1])).reduceByKey(lambda x, y: x + y).collect()

        # Calculate the cost of Round i
        for index in range(k):
            costs[i] += centroids_cost[index][1]
        
        # Calculate the number of nodes in each cluster
        num_nodes_in_cluster = clustered_node.countByKey()

        # Update the centroids
        new_centroids = clustered_node.reduceByKey(lambda x, y: x + y).map(lambda x: (x[0], x[1]/num_nodes_in_cluster[x[0]])).collect()
        
        # Allocate new new centroids into global centroids
        for centroid in new_centroids:
            centroids[centroid[0]] = centroid[1]
    
    improvement = (costs[0] - costs[-1])/ costs[0] * 100
    costs = costs[1:]
    
    return costs, centroids, improvement

def plot_losses(png_save_path, c1_losses, c2_losses, max_iter, method):
    x = ['Round {}'.format(i) for i in range(1, max_iter + 1)]
    plt.title(method)
    plt.plot(x, c1_losses, label='c1')
    plt.plot(x, c2_losses, label='c2')
    plt.legend(loc='upper right')
    plt.savefig(png_save_path)
    plt.close()

def centroid_pair_dist(file_path, centroids, method):
    with open(file_path, "w", newline='') as csvfile:
        centroid_num = len(centroids)
        writer = csv.writer(csvfile)
        writer.writerow([method] + list(range(1, centroid_num + 1)))
        for i in range(centroid_num):
            row = [i + 1]
            for j in range(centroid_num):
                if i > j:
                    row += ['']
                    continue
                elif i == j:
                    value = 0.00
                else:
                    if method == 'e': # Euclidean
                        value = np.sqrt(np.sum(np.power(np.array(centroids[i]) - np.array(centroids[j]), 2)))
                    elif method == 'm': # Manhattan
                        value = np.sum(np.abs(np.array(centroids[i]) - np.array(centroids[j])))
                    else:
                        raise NameError
                row += [np.round(value, 2)]
            writer.writerow(row)


if __name__ == "__main__":

    global centroids
    nodes_data = np.loadtxt("data.txt", usecols=range(0, col), delimiter=' ', dtype=np.float32)

    # Two methods to implement the K-means algorithm
    METHODS = ['e', 'm']

    improvement = []

    for METHOD in METHODS:
        MODE = METHOD
        
        # Using c1.txt centroid set
        print('\n////// c1 //////\n')
        c1_losses, c1_centroids, c1_improvement = k_means(nodes_data, k, iteration, "c1")

        # Using c2.txt centroid set
        print('\n////// c2 //////\n')
        c2_losses, c2_centroids, c2_improvement = k_means(nodes_data, k, iteration, "c2")

        # Using plt library to draw the plot digram
        plot_losses('result/{}.png'.format(METHOD), c1_losses, c2_losses, iteration, METHOD)

        # Using csv library to record the distance between centroid points
        centroid_pair_dist('result/{}_c1_{}.csv'.format(METHOD, 'e'), c1_centroids, 'e')
        centroid_pair_dist('result/{}_c1_{}.csv'.format(METHOD, 'm'), c1_centroids, 'm')
        centroid_pair_dist('result/{}_c2_{}.csv'.format(METHOD, 'e'), c2_centroids, 'e')
        centroid_pair_dist('result/{}_c2_{}.csv'.format(METHOD, 'm'), c2_centroids, 'm')

        improvement.append(c1_improvement)
        improvement.append(c2_improvement)
    
    print("c1 with Euclidean improvement: " + str(improvement[0]))
    print("c2 with Euclidean improvement: " + str(improvement[1]))
    print("c1 with Manhattan improvement: " + str(improvement[2]))
    print("c2 with Manhattan improvement: " + str(improvement[3]))



